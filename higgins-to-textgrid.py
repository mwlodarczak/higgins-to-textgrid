#!/usr/bin/env python
# -*- coding: utf-8 -*-

# higgins-2-textgrid -- Query and convert Higgins annotations to TextGrids.
# Copyright (C) 2013 Marcin Włodarczak
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from __future__ import print_function, division

import sys
import os
import argparse
from collections import defaultdict

from lxml import etree

import tgt

# Some constants
HIGGINS_NS = 'http://www.speech.kth.se/higgins/2005/annotation/'

# A temporary fix for imprecisise timestamps in the annotations
tgt.Time._precision = 0.01

def get_all_features(node):
    """Return names of all features annotated in a file."""
    
    feature_nodes = node.xpath('//ns:segment//ns:feature', namespaces={'ns': HIGGINS_NS})
    features_unique = set(x.attrib['name'] for x in feature_nodes)
    return list(features_unique)

def get_tracks(root):
    """Return names of all tracks annotated in a file"""
    track_nodes = root.xpath('//ns:track', namespaces={'ns': HIGGINS_NS})
    return [x.attrib['id'] for x in track_nodes]

def get_features(node):
    """Return all features of a segment."""
    return node.xpath('./ns:features/ns:feature', namespaces={'ns': HIGGINS_NS})

def parse_transcription(node, res=None):
    """Recursively parse and concatenate all transcriptions of a segment
    and its subsegments."""

    if res is None:
        res = []

    # Get the next node
    try:
        current = node[0]
    except IndexError:
        current = None

    if current is None:
        return ' '.join(res)
    elif current.tag == '{%s}t' % HIGGINS_NS:
        res.append(current.text)
        return parse_transcription(node[1:], res)
    elif current.tag == '{%s}segment' % HIGGINS_NS:
        # Recursively parse subsegments
        transcription = current.find('ns:transcription', namespaces={'ns': HIGGINS_NS})
        trans_embed = parse_transcription(transcription)
        features = get_features(current)
        # TODO: if a subsegment specifies start and end times, 
        # return separate interval objects for each feature.
        # Otherwise wrap transcription in <feature> or <feature val=value> tags
        opening_tags = '<' + '><'.join(['%s val=%s' % (x.attrib['name'], x.text) 
                                        for x in features]) + '>'
        closing_tags = '</' + '></'.join([x.attrib['name'] for x in features]) + '>'
        res.append(opening_tags + trans_embed + closing_tags)
        return parse_transcription(node[1:], res)
    else:
        # Only <segment> and <t> tags are allowed within <transcription> nodes.
        raise Exception('Unexpected tag: %s' % current.tag)
        
def higgins_to_tg(ann_root, include_features=None, exclude_features=None, 
               include_tracks=None, exclude_tracks=None):
    """Convert annotations in the Higgins XML format to Praat TextGrids.
    Features and tracks to include or exclude can be specified."""

    # defaultdict to store all annotation tiers in. NB: as defaultdict 
    # does not allow passing arguments to the constructor, at this point
    # the tiers have no names. That will be fixed later on when the final
    # TextGrid is constructed.
    tiers = defaultdict(lambda: tgt.IntervalTier())

    # Get session_metadata
    session_metadata_node = ann_root.find('ns:features', namespaces={'ns': HIGGINS_NS})
    session_metadata = [x.attrib['name'] for x in session_metadata_node]

    # Get segments
    segments = ann_root.find('ns:segments', namespaces={'ns': HIGGINS_NS})

    for seg in segments:
        start_time = float(seg.attrib['start'])
        end_time = float(seg.attrib['end'])
        track = seg.attrib['track']
        seg_id = seg.attrib['id']

        if ((include_tracks is not None and track not in include_tracks)
            or (exclude_tracks is not None and track in exclude_tracks)):
            continue
        
        # Store segment timestamps and its ID
        tiers['%s-segments' % track].add_interval(tgt.Interval(start_time, end_time, seg_id))
        
        # Parse segment transcription (including embedded subsegment)
        trans_node = seg.find('ns:transcription', namespaces={'ns': HIGGINS_NS})
        if trans_node is not None:
            trans = parse_transcription(trans_node, res=[])
            tiers['%s-trans' % track].add_interval(tgt.Interval(start_time, end_time, trans))

        # Get all features of a segment
        seg_features = get_features(seg)

        if not seg_features:
            continue

        # Concatenate values of all instances of the same feature
        seg_features_unique = defaultdict(list)
        for feature in seg_features:
            feature_name = feature.attrib['name']
            # If no feature value is specified (i.e. if the tag has no text),
            # default to True
            feature_value = feature.text if feature.text is not None else 'True'

            if ((include_features is not None and feature_name not in include_features)
                or (exclude_features is not None and feature_name in exclude_features)):
                continue

            seg_features_unique[feature_name].append(feature_value)

        for feature in seg_features_unique:
            tiers['%s-%s' % (track, feature)].add_interval(tgt.Interval(start_time, end_time, ','.join(seg_features_unique[feature])))

    # Generate the final TextGrid
    tg = tgt.TextGrid()
    for tier_name in sorted(tiers):
        # Fix tier names.
        tiers[tier_name].name = tier_name
        tg.add_tier(tiers[tier_name])
    return tg

def parse_arguments():
    """Parse command-line arguments."""
    argparser = argparse.ArgumentParser(description='Query and convert annotations in the Higgins XML format to Praat TextGrids.')
    argparser.add_argument('--input-files', type=str, nargs='+', required=True, dest='input_files',
                           help='Space-separated list of paths to XML files')
    argparser.add_argument('--include-features', type=str, nargs='+', required=False, dest='features_included',
                           help='Space-separated list of features to be included in the output TextGrid')
    argparser.add_argument('--exclude-features', type=str, nargs='+', required=False, dest='features_excluded',
                           help='Space-separated list of features to be excluded from the output TextGrid')
    argparser.add_argument('--include-tracks', type=str, nargs='+', required=False, dest='tracks_included',
                           help='Space-separated list of tracks to be included in the output TextGrid')
    argparser.add_argument('--exclude-tracks', type=str, nargs='+', required=False, dest='tracks_excluded',
                           help='Space-separated list of tracks to be included in the output TextGrid')
    argparser.add_argument('--output-encoding', type=str, nargs=1, dest='output_encoding', default='utf-8',
                           help='File encoding for the output TextGrid (defaults to UTF-8).')
    argparser.add_argument('--output-dir', type=str, nargs=1, dest='output_dir',
                           help='TextGrid format: short, long (defaults to short)')
    argparser.add_argument('--textgrid-format', type=str, nargs=1, dest='textgrid_type', choices=['short', 'long'],
                           default='short', help='TextGrid format: short, long (defaults to short)')
    argparser.add_argument('--list-features', action='store_true', dest='list_features',
                           help='List all features found in the input files without producing a TextGrid')
    argparser.add_argument('--list-tracks', action='store_true', dest='list_tracks',
                           help='List all tracks found in the input files without producing a TextGrid')
    return vars(argparser.parse_args())

def main():
    args = parse_arguments()
    
    for input_path in args['input_files']:

        #Parse the input file
        annotation_root = etree.parse(input_path).getroot()
        
        # Only do the conversion if neither the --list-features nor the --list-tracks
        # command-line arguments have been passed.
        if not (args['list_features'] or args['list_tracks']):

            # Unless an output_dir is specified, save output files to the directory of the input file
            input_dir, input_filename = os.path.split(input_path)
            if input_filename[-4:] == '.xml':
                output_filename = input_filename[:-4] + '.TextGrid'
            else:
                output_filename = input_filename + '.TextGrid'
            if args['output_dir'] is not None:
                output_path = os.path.join(args['output_dir'][0], output_filename)
            else:
                output_path = os.path.join(input_dir, output_filename)

            textgrid = higgins2tg(annotation_root, args['features_included'], args['features_excluded'],
                               args['tracks_included'], args['tracks_excluded'])
            tgt.write_to_file(textgrid, output_path, encoding=args['output_encoding'], format=args['textgrid_type'])            

        elif args['list_features']:
            features_all = get_all_features(annotation_root)
            sys.stdout.write('%s -- Features: ' % input_filename+ ', '.join(features_all) + '\n')

        else:
            tracks = get_tracks(annotation_root)
            sys.stdout.write('%s -- Tracks: ' % input_filename + ', '.join(tracks) + '\n')


if __name__ == "__main__":
    main()
