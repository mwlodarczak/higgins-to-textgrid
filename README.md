# higgins-to-textgrid

A small Python script for querying and converting annotations in the [Higgins](http://www.speech.kth.se/higgins/) [XML format](http://www.speech.kth.se/higgins/2005/annotation/annotation.xsd) to Praat TextGrids. 

Requires [TextGridTools] (https://github.com/hbuschme/TextGridTools).
